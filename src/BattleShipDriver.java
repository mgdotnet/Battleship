import java.util.Scanner;

public class BattleShipDriver {

    public static void main(String[] args)throws java.lang.Exception {
        Scanner myScan = new Scanner(System.in);

        int myRShot;
        int myCShot;

        BattleShip myBattle = new BattleShip();
        System.out.println("Let's play some BattleShip!");
        System.out.println(" ");
        System.out.println("'.' stands for unsearched water.");
        System.out.println("'~' stands for searched water.");
        System.out.println("'B' means you hit a ship!");
        System.out.println(" ");
        System.out.println(" ");
        System.out.println("HAVE FUN!!!");
        System.out.println(" ");
        System.out.println(" ");
        char[][] ocean = {

                {  'X', '.', '.', 'X'},
                {  '.', '.', 'X', '.'},
                {  '.', 'X', '.', '.'},
                {  '.', '.', '.', 'X'}

        };

        myBattle.BattleShip(ocean);
        System.out.println(myBattle.toString());

        boolean startGame = true;
        while(startGame){
            System.out.print("Enter a row number: ");
            myRShot = myScan.nextInt();

            System.out.print("Enter a column number: ");
            myCShot = myScan.nextInt();


            if (myBattle.isHit(myRShot, myCShot) == true){
                System.out.println(" ");
                System.out.println("YES!!! YOU GOT ONE!!!!");
                System.out.println(" ");
            }else {
                System.out.println(" ");
                System.out.println("WHOOPS - YOU MISSED! TRY AGAIN.");
                System.out.println(" ");
            }

            System.out.println(myBattle.toString());

            if (myBattle.toString().contains("All")){
                startGame = false;
            }
        }



    }


}
