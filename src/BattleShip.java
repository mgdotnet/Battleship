public class BattleShip {

    // Variable
    char[][] ocean = new char[4][4];



    public void BattleShip(char[][] inOcean)
    {
        this.ocean = inOcean;
    }

    public boolean isHit(int row, int col)
    {
        boolean myResult;
        if ( (row > this.ocean.length) ||
                (col > this.ocean[row].length)) {
            return false;
        }
        if ( ocean[row][col] == 'X' ) {
            this.ocean[row][col] = 'B';
            myResult = true;
        } else if (ocean[row][col] == '.' ) {
            this.ocean[row][col] = '~';
            myResult = false;
        } else {
            myResult = false;
        }
        return myResult;
    }

    public String toString()
    {

        int numberOfX = 0;
        String boardOut = " ";

        for ( int i = 0; i < this.ocean[0].length; i++) {
            boardOut = boardOut + "\t" + i;
        }

        boardOut = boardOut + "\n";

        for ( int i = 0; i < this.ocean[0].length; i++) {
            boardOut = boardOut + i;
            for ( int j = 0; j < this.ocean[i].length; j++) {
                if ( this.ocean[i][j] == 'X' ) {
                    boardOut = boardOut + "\t.";
                    numberOfX++;
                } else {
                    boardOut = boardOut + "\t" + this.ocean[i][j];
                }
            }
            boardOut = boardOut + "\n";
        }

        if (numberOfX == 0) {
            boardOut = boardOut + "All ships have been found - AWESOME.";
        }
        return boardOut;
    }


}


